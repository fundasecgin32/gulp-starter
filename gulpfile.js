// gulp.task('task-name', function() {     gulp.src('source-files')  1
// .pipe(gulpPluginFunction())  2     .pipe(gulp.dest('destination'));  3   });
// 'use strict';
var requirejs = require('requirejs');
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var sass = require("gulp-sass");
var log = require('gulplog');
var concat = require('gulp-concat');
var clean = require('gulp-clean');
const imagemin = require('gulp-imagemin');
let cleanCSS = require('gulp-clean-css');

/*
  -- TOP LEVEL FUNCTIONS --
  gulp.task - Define tasks
  gulp.src - Point to files to use
  gulp.dest - Points to folder to output
  gulp.watch - Watch files and folders for changes
*/

// Logs Message
gulp.task('intro', function () {
    var message_str = ['Gulp is running ', 'Use "gulp --tasks" to see the list of available tasks '].join(
        ''
    );

    return console.log(message_str);
});

// SASS to CSS Compile Sass
gulp.task('compile_css', function () {
    gulp
        .src('src/Sass/*.scss') // path to sass and scss files
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('app/assets/styles'));
});

// sass to css
gulp.task('css', function () {
    gulp
        .src('app/assets/styles')
        .pipe(sass()) //compile if need be
        .pipe(cleanCSS('style.css')) //concat
        .pipe(gulp.dest('dist/assets/styles'));
});

// Moving files from src to > app (dev) folder
gulp.task('to_app', function () {
    gulp.src('src/scripts/*.js') // path to  JS files
        .pipe(concat('all.js'))
        .pipe(gulp.dest('app/assets/scripts'));

    gulp.src('src/Sass/*') // path to  css files
    // .pipe(sass())  compile css
        .pipe(gulp.dest('app/assets/styles')); // move sass

    gulp.src('src/**/*.html') // path to html files //
        .pipe(gulp.dest('app/'));

    gulp.src('src/images/*.{img,png,ico,jpeg,svg}') // path to images files
        // .pipe(imagemin) // optimization
        .pipe(gulp.dest('app/assets/images'))
        .pipe(imagemin());
});

// Moving files from app to > dist(production) folder
gulp.task('to_dist', function () {
    gulp
        .src('app/assets/scripts/*.js') // path to  JS files
        .pipe(concat('all.js')) //concat
        .pipe(gulp.dest('dist/assets/scripts')); //move

    gulp
        .src('app/Sass/*') // path to  css files
        .pipe(sass()) //compile .scss, .sass to .css
        .pipe(concat('style.css')) // concat in onefile
        .pipe(gulp.dest('dist/assets/styles')); // move files

    gulp
        .src('app/*.html') // path to html files
        // .pipe(concat('index.html')) //concat
        .pipe(gulp.dest('dist/')); //move

    gulp
        .src('app/assets/images') // path to images files
        .pipe(gulp.dest('dist/assets/images'));

});

// Clean "dist" production folder
gulp.task('clean-dist', function () {
    return gulp
        .src('./dist/*', {read: true})
        .pipe(clean());
});
// Clean "app" production folder
gulp.task('clean-app', function () {
    return gulp
        .src('./app/*', {read: true})
        .pipe(clean());
});

// Minimize JS минификация и конкатенация всех js файлов в один (scrypt.js)
gulp.task('uglify-js', function () {
    gulp
        .src('dist/assets/scripts/*.js')
        .pipe(uglify())
        .pipe(concat('all.js'))
        .on('error', function (e) {
            console.log(e);
        })
        .pipe(gulp.dest('dist/assets/scripts'));
});

// Minimize минификация и конкатенация всех css файлов в один (style.css), взяли
// все файлы из app, отправили файл в dist
gulp.task('minify-css', () => {
    return gulp
        .src('app/assets/styles/*.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist/assets/styles'));
});

// Optimize Images
gulp.task(
    'imageMin',
    () => gulp.src('app/images/*').pipe(imagemin()).pipe(gulp.dest('app/images/'))
);

//  DEFAULT -------------------------------
gulp
    .task('default', [
        'compile_css',
        'to_dist',
        'uglify-js',
        'minify-css',
        'clean-dist',
        'imageMin'
    ])
    .on('error', function (e) {
        console.log(e);
    });

// gulp.task('watch', function(){   gulp.watch('src/scripts/*.js',
// ['uglify-js']);   gulp.watch('src/images/*', ['imageMin']);
// gulp.watch('src/styles/*.scss', ['sass']);   gulp.watch('src/**/*.html',
// ['move']);   gulp.watch('src/img/*', ['imageMin']); });

gulp.task('build:dev', ['intro', 'clean-app', 'compile_css', 'to_app', 'imageMin']);
gulp.task('build:prod', ['intro','clean-dist' ,'imageMin', 'css', 'to_dist', 'uglify-js', 'minify-css']);